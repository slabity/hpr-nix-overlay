self: super:
{
  hpr = {
    Fabric = super.python2Packages.Fabric.overridePythonAttrs (oldAttrs: rec {
      pname = "Fabric";
      version = "1.14.1";

      src = super.python2Packages.fetchPypi {
        inherit pname version;
        sha256 = "1a3ndlpdw6bhn8fcw1jgznl117a8pnr84az9rb5fwnrypf1ph2b6";
      };

      checkPhase = ''
        touch tests/main.py
      '';
    });


    riskconsole = super.callPackage ./wrapper.nix {
      wrapperName = "riskconsole";
      binArg = "-newriskconsole";
    };

    monitoringconsole = super.callPackage ./wrapper.nix {
      wrapperName = "monitoringconsole";
      binArg = "-newmonitor";
    };

    ausconsole = super.callPackage ./wrapper.nix {
      wrapperName = "ausconsole";
      binArg = "-newriskconsole";
    };
  };
}
