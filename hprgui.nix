{ stdenv, fetchurl, unzip }:

stdenv.mkDerivation rec {
  name = "hprgui-unwrapped-${version}";
  version = "3.4.131";
  date = "18Oct2019";

  src = fetchurl {
    url = "http://hpr-share/Builds/gui/riskconsole/release-${version}-${date}/HPR-RiskConsole-${version}.zip";
    sha256 = "0w6p7n9dszxha32gs12hcmi309715fwr759hd9pi4wr7hsgkzris";
  };

  buildInputs = [ unzip ];

  installPhase = ''
    mkdir -p $out/share/java
    mv hprRmGUI.jar $out/share/java
    mv lib/* $out/share/java/
  '';

  meta = with stdenv.lib; {
    description = "a set of java objects for running HPR GUI programs";
    license = licenses.unfree;
    platforms = platforms.linux;
    maintainers = [ "Tyler Slabinski" ];
  };
}
